( function(){
	angular.module( 'agenda.model' )
	
	.factory( 'ContatoModel',
		[ "Storage",

		function( Storage )
		{	
			var contatos = Storage.getObject( 'contatos', true ) ;

			var updateList = function() {
				Storage.setObject( 'contatos', contatos ) ;
			}

			var metodos = {
				listarContatos: function() {
					return contatos ;
				},
				listarFavoritos: function() {
					favoritos = [] ;

					for( idx in contatos ) {
						if( contatos[idx].favorito )
							favoritos.push( contatos[idx] ) ;
					}

					return favoritos ;
				},
				guardar: function( form ) {
					if( form.id )
					{
						idx = metodos.getIdx( form.id ) ;
						contatos[idx] = form ;
					}
					else
					{
						if( contatos.length )
							form.id = contatos[contatos.length-1].id + 1 ;
						else
							form.id = 1 ;

						contatos.push( form ) ;
					}

					updateList() ;
				},
				remover: function( id ) {
					idx = metodos.getIdx( id ) ;
					contatos.splice( idx, 1 ) ;
					
					updateList() ;
				},
				getIdx: function( id ) {
					for( idx in contatos ) {
						contato = contatos[idx] ;
						
						if( contato.id == id )
							return idx ;
					} ;

					return null ;
				}
			}

			// Métodos Públicos
			return metodos ;
		}]
	)
})() ;
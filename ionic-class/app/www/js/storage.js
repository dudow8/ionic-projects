( function(){
	angular.module( 'agenda.service' )

	.factory( "Storage", 
		[ '$window',
		function( $window ) {
			return {
				set: function( key, value )
				{
					$window.localStorage[key] = value ;
				},
				get: function( key, defaultValue )
				{
					return $window.localStorage[key] || defaultValue ;
				},
				setObject: function( key, value )
				{
					$window.localStorage[key] = JSON.stringify( value ) ;
				},
				getObject: function( key, collection )
				{
					defaultObject = collection ? '[]' : '{}' ;
					return JSON.parse( $window.localStorage[key] || defaultObject ) ;
				},
				delete: function( key )
				{
					if( $window.localStorage[key] )
						$window.localStorage.removeItem( key ) ;
				}
			}
		}
	])
})() ;
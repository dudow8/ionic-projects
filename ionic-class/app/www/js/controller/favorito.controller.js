( function(){
	angular.module( 'agenda.controller' )

	.controller( 'FavoritoController',
			[ 'ContatoModel', 'Cache', '$state',
		function( ContatoModel, Cache, $state )
		{
			this.favoritos = ContatoModel.listarFavoritos() ;
		}
	])
})() ;
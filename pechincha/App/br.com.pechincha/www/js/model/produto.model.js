( function(){
	angular.module( 'pechincha.model' )
	
	.factory( 'ProdutoModel', 
		[ '$database', '$localstorage', '$filter', 'DBQuery',
	function( $database, $localstorage, $filter, DBQuery )
	{
		// TODO :: Criar uma execução do Promisse direto na camada do $database
		return {
			listAllByProposta: function( proposta ) {
				var promisse = $database.query({
					sql: DBQuery.prepareSelect({
						table: "Produto",
						select: [ '*' ],
						where: [
							"Produto.CodPechincha = ?",
							"AND",
							"Produto.CodProposta = ?",
						],
						order: [ "Produto.Pdc DESC" ]
					}),
					data: [ proposta.CodPechincha, proposta.CodProposta ],
				}) ;
				
				return promisse ;
			},
			// TODO :: Tratar todos os erros
			insert: function( form, callback ) {
				// Faz o update da Chave
				$database.query( DBQuery.pkSelfSql( "Produto", 'CodProduto', 6, [ "CodPechincha = ?", "AND", "CodProposta = ?" ], [ form.CodPechincha, form.CodProposta ] ) )
				.then( function( PK ) {
					// Atribui o valor da Chave ao Form
					form.CodProduto = PK.objects[0].PK ;
					form.Data = ( $filter( 'date' )( new Date(), "yyyy-MM-dd" ) ) ;
					// Faz o insert
					$database.query( DBQuery.insertSql( "Produto", form ) )
					.then( function(){
						// Prepara o objeto de Retorno
						$database.query({ sql: DBQuery.prepareSelect({ table: "Produto", where: [ "CodPechincha = ?", "AND", "CodProposta = ?", "AND", "CodProduto = ?" ] } ), data: [ form.CodPechincha, form.CodProposta, form.CodProduto ] })
						.then( function( inserted ){
							// Executa o Callback com o objeto  Inserido via Parâmetro
							if( typeof( callback ) == "function" )
					   			callback( inserted.objects[0] ) ;
						}, function(){
							console.log( "Fetch Object Error" ) ;
						}) ;
					}, function(){
						console.log( "Insert Error" ) ;
					}) ;
				}, function(){
					console.log( "Fetch PK Error" ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			remove: function( Produto, callback ) {
				$database.query( DBQuery.deleteSql({
					table: "Produto",
					where: [ "CodPechincha = ?", "AND", "CodProposta = ?", "AND", "CodProduto = ?" ],
					data: [ Produto.CodPechincha,  Produto.CodProposta,  Produto.CodProduto ]
				}))
				.then( function() {
					if( typeof( callback ) == "function" ) callback( true ) ;
				}, function() {
					if( typeof( callback ) == "function" ) callback( false ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			update: function( form, callback ) {
				$database.query( DBQuery.updateSql( "Produto", form, [ "Produto.CodPechincha = ?", "AND", "Produto.CodProposta = ?", "AND", "Produto.CodProduto = ?" ], [ form.CodPechincha, form.CodProposta, form.CodProduto ] ) )
				.then( function() {
					// Prepara o objeto de Retorno
					$database.query({ sql: DBQuery.prepareSelect({ table: "Produto", where: [ "Produto.CodPechincha = ?", "AND", "Produto.CodProposta = ?", "AND", "Produto.CodProduto = ?" ] }), data: [ form.CodPechincha, form.CodProposta, form.CodProduto ] })
					.then( function( updated ) {
						// Executa o Callback com o objeto  Inserido via Parâmetro
						if( typeof( callback ) == "function" )
				   			callback( updated.objects[0] ) ;
					}, function() {
						console.log( "Fetch Object Error" ) ;
					}) ;
				}, function() {
					console.log( "Update Error" ) ;
				}) ;
			},
			// Returns the best offer
			bestOffer: function( pechincha, callback ) {
				$database.query({
					sql: DBQuery.prepareSelect({
						table: "Produto",
						select: [
							"Produto.Pdc as pPdc",
							"Produto.Foto as pFoto",
							"Produto.Nome as pNome",
							"Produto.Preco as pPreco",
							"Produto.Desconto as pDesconto",
							"Produto.FormaPagamento as pFormaPagamento",
							"Produto.Parcelas as pParcelas",
							"Produto.Quantidade as pQuantidade",
							"Produto.Anotacoes as pAnotacoes",
							"Fornecedor.Nome as fNome",
							"Fornecedor.Endereco as fEndereco",
							"Fornecedor.Telefone as fTelefone",
							"Fornecedor.Email as fEmail"
						],
						where: [
							"Produto.CodPechincha = ?"
						],
						join: [{
							entity: "Proposta",
							mode: "LEFT JOIN",
							conditions: [
								"Produto.CodPechincha = Proposta.CodPechincha",
								"AND",
								"Produto.CodProposta = Proposta.CodProposta"
							]
						},{
							entity: "Fornecedor",
							mode: "LEFT JOIN",
							conditions: [ "Proposta.CodFornecedor = Fornecedor.CodFornecedor" ]
						}],
						order: [ 
							"Produto.Pdc DESC", 
							"Produto.Preco ASC", 
							"Produto.Desconto DESC", 
							"Produto.Parcelas DESC"
						]
					}),
					data: [ pechincha.CodPechincha ],
				}).then( function( ret ){
					if( typeof( callback ) == 'function' )
						callback( ret.objects[0] ) ;
				}, function(){
					if( typeof( callback ) == 'function' )
						callback( null ) ;
				}) ;
			}
		}
	}])
})() ;
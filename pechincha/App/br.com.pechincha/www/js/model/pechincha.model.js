( function(){
	angular.module( 'pechincha.model' )
	
	.factory( 'PechinchaModel', 
		[ '$database', '$localstorage', '$filter', 'DBQuery',
	function( $database, $localstorage, $filter, DBQuery )
	{
		// TODO :: Criar uma execução do Promisse direto na camada do $database
		return {
			listAll: function() {
				var promisse = $database.query({
					sql: DBQuery.prepareSelect({
						table: "Pechincha",
						select: [ '*', "(" +
							DBQuery.prepareSelect({
								table: "Proposta",
								select: [ 'COUNT(*)'],
								where: [ "Proposta.CodPechincha = Pechincha.CodPechincha" ]
							})
						+" ) AS Locked" ],
						order: [ "Pechincha.Concluido ASC", "Pechincha.Data DESC" ]
					}),
					data: [],
				}) ;
				
				return promisse ;
			},
			// TODO :: Tratar todos os erros
			insert: function( form, callback ) {
				// Faz o update da Chave
				$database.query( DBQuery.pkSelfSql( "Pechincha", 'CodPechincha', 6, [ "1 = 1" ], [] ) )
				.then( function( PK ) {
					// Atribui o valor da Chave ao Form
					form.CodPechincha = PK.objects[0].PK ;
					form.Data = ( $filter( 'date' )( new Date(), "yyyy-MM-dd" ) ) ;
					// Faz o insert
					$database.query( DBQuery.insertSql( "Pechincha", form ) )
					.then( function(){
						// Prepara o objeto de Retorno
						$database.query({ sql: DBQuery.prepareSelect({ table: "Pechincha", where: [ "Pechincha.CodPechincha = ?" ] } ), data: [ form.CodPechincha ] })
						.then( function( inserted ){
							// Executa o Callback com o objeto  Inserido via Parâmetro
							if( typeof( callback ) == "function" )
					   			callback( inserted.objects[0] ) ;
						}, function(){
							console.log( "Fetch Object Error" ) ;
						}) ;
					}, function(){
						console.log( "Insert Error" ) ;
					}) ;
				}, function(){
					console.log( "Fetch PK Error" ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			remove: function( Pechincha, callback ) {
				$database.query( DBQuery.deleteSql({
					table: "Pechincha",
					where: [ "CodPechincha = ?" ],
					data: [ Pechincha.CodPechincha ]
				}))
				.then( function() {
					if( typeof( callback ) == "function" ) callback( true ) ;
				}, function() {
					if( typeof( callback ) == "function" ) callback( false ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			update: function( form, callback ) {
				$database.query( DBQuery.updateSql( "Pechincha", form, [ "Pechincha.CodPechincha = ?" ], [ form.CodPechincha ] ) )
				.then( function() {
					// Prepara o objeto de Retorno
					$database.query({ sql: DBQuery.prepareSelect({ table: "Pechincha", where: [ "Pechincha.CodPechincha = ?" ] }), data: [ form.CodPechincha ] })
					.then( function( updated ) {
						// Executa o Callback com o objeto  Inserido via Parâmetro
						if( typeof( callback ) == "function" )
				   			callback( updated.objects[0] ) ;
					}, function() {
						console.log( "Fetch Object Error" ) ;
					}) ;
				}, function() {
					console.log( "Update Error" ) ;
				}) ;
			}
		}
	}])
})() ;
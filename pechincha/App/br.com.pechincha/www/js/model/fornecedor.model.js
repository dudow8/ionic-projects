( function(){
	angular.module( 'pechincha.model' )
	
	.factory( 'FornecedorModel', 
		[ '$database', '$localstorage', '$filter', 'DBQuery', 
	function( $database, $localstorage, $filter, DBQuery )
	{
		// TODO :: Criar uma execução do Promisse direto na camada do $database
		return {
			listAll: function() {
				var promisse = $database.query({
					sql: DBQuery.prepareSelect({
						table: "Fornecedor",
						select: [ '*', "(" +
							DBQuery.prepareSelect({
								table: "Proposta",
								select: [ 'COUNT(*)'],
								where: [ "Proposta.CodFornecedor = Fornecedor.CodFornecedor" ]
							})
						+" ) AS Locked"],
						order: [ "Fornecedor.Nome" ]
					}),
					data: [],
				}) ;
				
				return promisse ;
			},
			// TODO :: Tratar todos os erros
			insert: function( form, callback ) {
				// Faz o update da Chave
				$database.query( DBQuery.pkSelfSql( "Fornecedor", 'CodFornecedor', 6, [ "1 = 1" ], [] ) )
				.then( function( PK ) {
					// Atribui o valor da Chave ao Form
					form.CodFornecedor = PK.objects[0].PK ;
					form.Data = ( $filter( 'date' )( new Date(), "yyyy-MM-dd HH:mm:ss" ) ) ;
					// Faz o insert
					$database.query( DBQuery.insertSql( "Fornecedor", form ) )
					.then( function(){
						// Prepara o objeto de Retorno
						$database.query({ sql: DBQuery.prepareSelect({ table: "Fornecedor", where: [ "Fornecedor.CodFornecedor = ?" ] } ), data: [ form.CodFornecedor ] })
						.then( function( inserted ){
							// Executa o Callback com o objeto  Inserido via Parâmetro
							if( typeof( callback ) == "function" )
					   			callback( inserted.objects[0] ) ;
						}, function(){
							console.log( "Fetch Object Error" ) ;
						}) ;
					}, function(){
						console.log( "Insert Error" ) ;
					}) ;
				}, function(){
					console.log( "Fetch PK Error" ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			remove: function( Fornecedor, callback ) {
				$database.query( DBQuery.deleteSql({
					table: "Fornecedor",
					where: [ "CodFornecedor = ?" ],
					data: [ Fornecedor.CodFornecedor ]
				}))
				.then( function() {
					if( typeof( callback ) == "function" ) callback( true ) ;
				}, function() {
					if( typeof( callback ) == "function" ) callback( false ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			update: function( form, callback ) {
				$database.query( DBQuery.updateSql( "Fornecedor", form, [ "Fornecedor.CodFornecedor = ?" ], [ form.CodFornecedor ] ) )
				.then( function() {
					// Prepara o objeto de Retorno
					$database.query({ sql: DBQuery.prepareSelect({ table: "Fornecedor", where: [ "Fornecedor.CodFornecedor = ?" ] }), data: [ form.CodFornecedor ] })
					.then( function( updated ) {
						// Executa o Callback com o objeto  Inserido via Parâmetro
						if( typeof( callback ) == "function" )
				   			callback( updated.objects[0] ) ;
					}, function() {
						console.log( "Fetch Object Error" ) ;
					}) ;
				}, function() {
					console.log( "Update Error" ) ;
				}) ;
			}
		}
	}])
})() ;
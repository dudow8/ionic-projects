( function(){
	angular.module( 'ionic.utils', [] )
	
	// Gerenciador de armazenamento local da aplicação
	.factory( '$localstorage', 
		[ '$window', 
	function( $window ) {
		return {
			set: function( key, value )
			{
				$window.localStorage[key] = value ;
			},
			get: function( key, defaultValue )
			{
				return $window.localStorage[key] || defaultValue ;
			},
			setObject: function( key, value )
			{
				$window.localStorage[key] = JSON.stringify( value ) ;
			},
			getObject: function( key, collection )
			{
				defaultObject = collection ? '[]' : '{}' ;
				return JSON.parse( $window.localStorage[key] || defaultObject ) ;
			},
			delete: function( key )
			{
				if( $window.localStorage[key] )
					$window.localStorage.removeItem( key ) ;
			}
		}
	}])

	// Gerenciador de banco de dados da aplicação
	.factory( '$database', 
		[ '$q',
	function( $q )
	{
		// Armazena a instância do DB
		var db = null ;

		// Configurações do banco de dados
		var config = { version: 1.0, dbName: "app_db", dbDisplayName: "app_db", dbSize: ( 5 * 1024 * 1024 ) }

		// TODO :: Melhorar o bathcontroll
		// Controle de execuções em bath
		var bathControll = {} ;
		// Método para resetar o BathControll
		var resetBathControll = function() {
			bathControll = { queries: 0, sqlerror: 0, sqlsuccess: 0, lastindex: 0, } ;
		} ;
		// Método de execução de callback
		var callbackBathControll = function( callback )
		{
			// Incrementa a execução
			++bathControll.lastindex ;
			// Caso seja a úlma execução
			if( ( bathControll.lastindex == bathControll.queries )  && typeof( callback ) == "function" ) {
				callback({ 
					success: bathControll.sqlsuccess, 
					error: bathControll.sqlerror
				}) ;
			}
		} ;
		
		return {
			setup: function( data ) { angular.extend( config, data ) },
			open: function( callback )
			{
				// Caso o banco de dados não tenha sido instanciado
				if( db == null )
					db = openDatabase( config.dbName, config.version, config.dbDisplayName, config.dbSize ) ;

				return db ;
			},
			// Útil para SYNC, Database Tables CREATE and DROP
			bath: function( queries, callback )
			{
				// Update no bath controll
				resetBathControll() ;
				bathControll.queries = queries.length ;

				db.transaction( function( cn ) {
					angular.forEach( queries, function( query ) {
						// Executa a Query
						cn.executeSql( query.sql, query.data,
							function( sqlTransaction, sqlResultSet ) {
								// Incrementa os controles de execução
								++bathControll.sqlsuccess ;
								// Controla a execução do callback
								callbackBathControll( callback ) ;
							}, 
							function( sqlTransaction, sqlError ) {
								console.log( sqlError ) ;
								// Incrementa os controles de execução
							   	++bathControll.sqlerror ;
							   	// Controla a execução do callback
								callbackBathControll( callback ) ;
							}
						) ;
					}) ;
				}) ;
			},
			// TODO :: Melhorar e estudar sobre o $q e o promisse
			query: function( query, callback, error, context )
			{
				var deferred = $q.defer() ;
				
				db.transaction( function( cn ) {

					// console.log( query.sql ) ;
					// console.log( query.data ) ;

					cn.executeSql( query.sql, query.data,
						function( sqlTransaction, sqlResultSet )
						{
							var resultObj = {},
								responses = [],
								ret = {} ;
								
							if ( sqlResultSet != null && sqlResultSet.rows != null )
							{
								for ( var i = 0; i < sqlResultSet.rows.length; i++ ) {
									resultObj = sqlResultSet.rows.item(i);
									responses.push( resultObj ) ;
								}
							}
							else
								responses.push( resultObj ) ;

							ret.objects = responses ;
							ret.rawquery = sqlResultSet ;
							
							// Resolve o promisse
							deferred.resolve( ret ) ;
							
							// Solução antiga - ( Não ajudou com a situação assincrona )
						   	if( typeof( callback ) == "function" )
						   		callback( sqlResultSet, context ) ;
						}, 
						function( sqlTransaction, sqlError ) {
							console.log( sqlError ) ; // Temporário
						   if( typeof( error ) == "function" )
						   		error( sqlError, context ) ;
						}
					);
				}) ;

				return deferred.promise ;
			}
		} ;
	}])
	
	// Gerenciamento de serviços do celular
	.factory( '$appServices', 
		[  '$cordovaCamera',
	function( $cordovaCamera )
	{
		var states = [] ;
		var captureOptions = {} ;
		
		// Preenche a lista de status caso encontrados
		if( typeof( navigator.connection ) != 'undefined' )
	    {	
		    states[navigator.connection.UNKNOWN]  = 'UNKNOWN' ;
		    states[navigator.connection.ETHERNET] = 'ETHERNET' ;
		    states[navigator.connection.WIFI]     = 'WIFI' ;
		    states[navigator.connection.CELL_2G]  = '2G' ;
		    states[navigator.connection.CELL_3G]  = '3G' ;
		    states[navigator.connection.CELL_4G]  = '4G' ;
		    states[navigator.connection.CELL]     = 'GENERIC' ;
		    states[navigator.connection.NONE]     = 'NO-NETWORK' ;
	    }

	    // Opções para captura da imagem
	    if( typeof( Camera ) != "undefined" )
	    {
			captureOptions = {
				quality: 75,
				destinationType: Camera.DestinationType.FILE_URI,
				sourceType: Camera.PictureSourceType.CAMERA,
				allowEdit: true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 800,
				targetHeight: 600,
				correctOrientation: true,
				cameraDirection: Camera.Direction.BACK,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			} ;
	    }
	    
		return {
			/**
			 * Verifica o tipo de conexão do usuário.
			 * 	@return Tipo de Conexão do usuário
			 */
			checkConnection: function () 
			{
			    var ret = 'UNKNOWN' ;
			    
			    if( typeof( navigator.connection ) != 'undefined' )
			    {
			    	var networkState = navigator.connection.type ;
			    	
			    	if( typeof( states[networkState] ) != 'undefined' )
				    	ret = states[networkState] ;
			    }
			    
			    return ret ;
			},
			/**
			 * Captura uma imagem com parâmetros padão do App
			 * @param  {Function} onsucess Callback de chamada no caso de sucesso
			 * @param  {Function} onerror Callback de chamada no caso de erro
			 */
			pictureCapture: function ( onsucess, onerror ) {
				if( typeof( onsucess ) == "function" ) {
					// Verifica se o dispositivo tem o recurso da câmera
					if( typeof( Camera ) != "undefined" ) {
						// Faz a chamada para a API de capturar foto
						$cordovaCamera.getPicture( self.captureOptions ).then( function( imageURI ) {
							// Força o redimensionamento da imagem
							var canvas = document.createElement( 'CANVAS' ) ;
							var ctx = canvas.getContext( '2d' ) ;
							var img = new Image() ;

							img.crossOrigin = 'Anonymous' ;

							img.onload = function() {
								var dataURL ;
								var cw = img.width, ch = img.height, cx = 0, cy = 0, aratio = 1 ;

								cw = img.width ;
								ch = img.width ;
								aratio = cw / ch ;

								// canvas.width = img.width * 0.1 ;
								// canvas.height = img.height * 0.1 ;

								canvas.width = 400 ;
								canvas.height = ( img.height * canvas.width ) / img.width ;
								
								ctx = canvas.getContext( '2d' ) ;
								ctx.drawImage( img, 0, 0, canvas.width, canvas.height ) ;
								
								dataURL = canvas.toDataURL( 'image/jpeg', 0.8 ) ;

								// Executa o callback de sucesso
								onsucess( dataURL ) ;

								canvas = null ; 
							} ;

							img.src = imageURI ;

						}, function( err ) {
							// Executa o callback de erro
							onerror( err ) ;
						}) ;
					}
					else
						// Executa o callback de erro
						onerror( 'NO-CAMERA' ) ;
				}
			}
		} ;
	}])
})() ;
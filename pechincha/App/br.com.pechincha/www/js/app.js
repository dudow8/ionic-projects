( function(){
	// Declara as dependências
	angular.module( 'pechincha.controller', [] ) ;
	angular.module( 'pechincha.model', [] ) ;
	
	// Instância do App
	angular.module( 'pechincha', [
		'ionic',
		'ngCordova',
		'ionic.utils',
		'pechincha.pickup',
		'pechincha.model',
		'pechincha.controller',
	])
	
	// Executa o App
	.run( function( $ionicPlatform, $cordovaStatusbar, $database, $state, $ionicHistory ) {
		// Configura o Database
		$database.setup({
			dbName: "pechincha",
			version: 1,
			dbDisplayName: "pechincha_db",
			// dbSize: ( 100 * 1024 * 1024 ) // 100MB
			dbSize: ( 50 * 1024 * 1024 ) // 50MB
		}) ;
		
		// Executa o Database
		$database.open( function(){} ) ;
		
		// setTimeout( function() {
	 //        navigator.splashscreen.hide() ;
	 //    }, 100 ) ;
		
		// Configura a plataforma
		$ionicPlatform.ready( function()
		{
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
			if( window.cordova && window.cordova.plugins.Keyboard ) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar( true ) ;
			}
			
			// TODO
			// if( window.StatusBar ) {
			// 	StatusBar.overlaysWebView( false ) ;
			// 	StatusBar.styleLightContent();
			// 	StatusBar.backgroundColorByHexString( '#00695C' ) ;
			// }
			
			// Configurações para o botão back do Android
		  	$ionicPlatform.registerBackButtonAction( function( event ) {
				switch( $state.current.name )
				{
					case "h-app.h-tabs.home":
						navigator.app.exitApp() ;
					break ;
					
					default:
						$ionicHistory.goBack() ;
					break ;
				}
			}, 100 ) ;
		});
	})
	
	/**
	 * Configurações de comportamento do Ionic
	 */
	.config( 
		[ '$ionicConfigProvider',
	function( $ionicConfigProvider )
	{
		$ionicConfigProvider.backButton.previousTitleText( false ) ;
		$ionicConfigProvider.backButton.text( "" ) ;
		// $ionicConfigProvider.views.transition( "none" ) ;
		
		// Habilita o scroll nativo
		// $ionicConfigProvider.scrolling.jsScrolling( false ) ;
		$ionicConfigProvider.scrolling.jsScrolling( true ) ;
	}])
	
	/**
	 * Configura as Rotas
	 */
	.config(
		[ '$stateProvider', '$urlRouterProvider', 
	function( $stateProvider, $urlRouterProvider ) 
	{
		// Não executa, pois o AppController que irá decidir para qual $state será direcionado
	  	// $urlRouterProvider.otherwise( '/' ) ;
	  	
		$stateProvider
		
		// Container do H-APP
		.state( "h-app", {
			url: "/h-app",
			abstract: true,
			templateUrl: "view/navigation/app.headerview.html",
		})
			// Container do H-TABS dentro de H-APP
			.state( "h-app.h-tabs", {
				url: "/h-tabs",
				abstract: true,
				views: {
					"happ-view": {
						templateUrl: "view/navigation/app.tabview.html",
						controller: "NavigationController as nvCtrl"
					}
				}
			})
			// View HOME dentro de h-app
			.state( "h-app.h-tabs.home", {
				url: "/home",
				// cache: false,
				views: {
					"app-home": {
						templateUrl: "view/app/home.html",
						controller: "AppController as appCtrl",
					}
				}
			})

			// View FORNECEDOR dentro de h-app
			.state( "h-app.h-tabs.fornecedor", {
				url: "/fornecedor",
				// cache: false,
				views: {
					"app-fornecedor": {
						templateUrl: "view/app/fornecedor-list.html",
						controller: "FornecedorController as frCtrl",
					}
				}
			})

			// View FORNECEDOR dentro de h-app
			.state( "h-app.h-tabs.about", {
				url: "/about",
				// cache: false,
				views: {
					"app-about": {
						templateUrl: "view/app/about.html"
					}
				}
			})
	}])
	
	/**
	 * Configurações para enviar os formulários via $http
	 */
	.config( 
		[ '$httpProvider',
	function( $httpProvider )
	{
		//Enable cross domain calls
		$httpProvider.defaults.useXDomain = true ;
		
		//Remove the header used to identify ajax call  that would prevent CORS from working
		delete $httpProvider.defaults.headers.common['X-Requested-With'] ;
		
		// Informs that parameter comes from a form
		$httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		
		// Format the form JSON before send via HTTP to Server
		$httpProvider.defaults.transformRequest.unshift( function ( data, headersGetter )
		{
			var key, result = [] ;
			
			for ( key in data )
			{
				if ( data.hasOwnProperty( key ) )
					result.push( encodeURIComponent( key ) + "=" + encodeURIComponent( data[key] ) ) ;
			}
			
			return result.join( "&" ) ;
		}) ;
	}])

})() ;
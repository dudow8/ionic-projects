( function(){
	angular.module( "pechincha.pickup", [] )
	
	.factory( "$pickup", 
		[ '$ionicModal', 
	function( $ionicModal ) {
		// Ponteiro interno para o próprio Serviço
		var self = this ;
		// Buffer dos modals
		self.modals = [] ;
		// Buffer dos calbacks
		self.calbacks = [] ;

		// Interface de chamadas públicas
		return {
			// Retorna a instância de um modal específico
			modal: function( key ) {
				if( typeof( self.modals[key] ) != "undefined" )
					return self.modals[key] ;
				else
					return null ;
			},
			// Remove a instancia de um modal
			remove: function( key ) {
				if( typeof( self.modals[key] ) != "undefined" ) {
					self.modals[key].remove() ;
					delete self.modals[key] ;
				}
			},
			// Executa o callback
			callback: function( key, param ) {
				if( typeof( self.calbacks[key] ) == 'function' ) {
					self.calbacks[key]( param ) ;
					delete self.calbacks[key] ;
				}
			},
			// Força uma entrada de modal
			putmodal: function( key, modal ) { 
				// Put the value inside Modal
				self.modals[key] = modal ;
			},
			// Força uma entrada de callback
			putcallback: function( key, calback, overwrite ) { 
				if( ( typeof( self.calbacks[key] ) != "undefined" && overwrite ) || typeof( self.calbacks[key] ) == "undefined" )
					self.calbacks[key] = calback ;
			},
			
			// Métodos para pickup de Fornecedor
			pickFornecedor: function( calback )
			{
				// Define o callback
				self.calbacks['calbackFornecedor'] = calback ;

				// Abre o Modal
				$ionicModal.fromTemplateUrl( 'view/app/fornecedor-pickup.html', { 
					animation: 'slide-in-up',
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Exibe o modal
					self.modals['pickUpFornecedor'] = modal ;
					self.modals['pickUpFornecedor'].show() ;
				});
			},
			
			// Métodos para pickup de Proposta
			pickProposta: function( calback )
			{
				// Define o callback
				self.calbacks['calbackProposta'] = calback ;

				// Abre o Modal
				$ionicModal.fromTemplateUrl( 'view/app/proposta-list.html', { 
					animation: 'slide-in-up',
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Exibe o modal
					self.modals['pickUpProposta'] = modal ;
					self.modals['pickUpProposta'].show() ;
				});
			},
			
			// Métodos para formProduto
			formProduto: function( calback )
			{
				// Define o callback
				self.calbacks['calbackProduto'] = calback ;
				
				// Abre o Modal
				$ionicModal.fromTemplateUrl( 'view/app/produto-form.html', { 
					animation: 'slide-in-up',
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Exibe o modal
					self.modals['pickUpProduto'] = modal ;
					self.modals['pickUpProduto'].show() ;
				});
			}
		}
	}])
})() ;
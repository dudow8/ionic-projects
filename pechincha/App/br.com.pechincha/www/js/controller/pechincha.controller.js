( function() {
	// Recupera o library de controller
	angular.module( "pechincha.controller" )
	
	// Instancia do controller
	.controller( 'PechinchaController', 
		[ '$scope', '$pickup', '$cordovaGeolocation', '$ionicModal', '$ionicPopup', 'CacheService', 'PechinchaModel', 
	function( $scope, $pickup, $cordovaGeolocation, $ionicModal, $ionicPopup, CacheService, PechinchaModel )
	{
		// Referência para o Controller ser reconhecido por métodos fora Contexto
		var self = this ;

		// Buffer da lista dos Pechinchas
		self.Pechinchas = [] ;

		// Buffer do registro de um Pechincha
		self.Pechincha = {} ;
		
		// Métodos para pickup
		self.modal = {
			// Modal para inclusão do Registro
			new: function() {
				$ionicModal.fromTemplateUrl( 'view/app/pechincha-form.html', { 
					animation: 'slide-in-up',
					scope: $scope,
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Reseta o formulário
					self.Pechincha = {} ;
					
					// Exibe o modal
					self.newPechinchaModal = modal ;
					self.newPechinchaModal.show() ;

					// Configura o ambiente do pickup caso não exista callback
					$pickup.putcallback( 'calbackPechincha', function( persisted ){
						self.init.run() ;
					}) ;
				});
			},
			// Modal para visão do Registro
			view: function( object ) {
				CacheService.set( "PropostaView", object ) ;
				$pickup.pickProposta( function(){
					self.init.run() ;
					CacheService.flush( "PropostaView" ) ;
				}) ;
			},
			// Modal para edição do Registro
			update: function( object ) {
				$ionicModal.fromTemplateUrl( 'view/app/pechincha-form.html', { 
					animation: 'slide-in-up',
					scope: $scope,
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Reseta o formulário
					angular.copy( object, self.Pechincha ) ;
					// self.Pechincha = object ;
					
					// Exibe o modal
					self.newPechinchaModal = modal ;
					self.newPechinchaModal.show() ;

					// Configura o ambiente do pickup
					$pickup.putcallback( 'calbackPechincha', function( persisted ) {
						self.init.run() ;
					}, true ) ;
				});
			},
			// Modal para visão do Registro
			remove: function( object ) {
				if( ! object.Locked ) {
					$ionicPopup.confirm({
						title: 'Atenção',
     					template: 'Confirma a remoção do Registro ?'
					}).then( function( confirm ){
						if( confirm ) {
							PechinchaModel.remove( object, function( isremoved ) {
								if( isremoved ) {
									self.init.run() ;
								}
								else {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível remover o Registro. Tente Novamente !"
									});
								}
							}) ;
						}
					}) ;
				}
				else
				{
					$ionicPopup.alert({
					    title: 'Atenção',
					    subTitle: 'O objeto não pode ser removido pois está sendo referenciado por outros registros'
					});
				}
			},
			// Fecha os detalhes da Rota e a inicia, caso marcada
			persist: function( frForm )
			{
				// Caso exista formulário para persistir os dados
				if( frForm )
				{
					// Container para os campos com erro
					var eFields = [] ;
					
					if( ! frForm.Alias.$valid ) eFields.push( "Alias" ) ;
					
					// Verifica se o formulário é válido
					if( ! eFields.length )
					{
						// Caso seja para fazer update
						if( self.Pechincha.CodPechincha )
						{
							// Remove as informações desnecessárias para o form
							delete self.Pechincha.Locked ;
							
							// Realiza o update
							PechinchaModel.update( self.Pechincha, 
								function( persisted ){
									$pickup.callback( 'calbackPechincha', persisted ) ;
									self.newPechinchaModal.hide().then( function( modal ) {
										self.newPechinchaModal.remove() ;
									}) ;
								}
							) ;
						}
						// Caso seja para fazer inclusão
						else
						{
							// Realiza o insert
							PechinchaModel.insert( self.Pechincha, 
								function( persisted ){
									// Fecha o modal e retorna como selecionado o ítem recém persistido
									self.newPechinchaModal.hide().then( function( modal ) {
										$pickup.callback( 'calbackPechincha', persisted ) ;
										$pickup.remove( 'pickUpPechincha' ) ;
										self.newPechinchaModal.remove() ;
									}) ;
								}, 
								function() {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível incluir o Registro. Tente Novamente !"
									});
								}
							) ;
						}
					}
					else
					{
						$ionicPopup.alert({
						    title: 'Formulário com Erro',
						    subTitle: 'Por favor, verifique os campos: ' + eFields.join( ', ' )
						});
					}
				}
				// Caso o formulário seja falso, descarta o form ou objeto em edição
				else
				{
					// Fecha o modal e cancela a operação
					self.newPechinchaModal.hide().then( function( modal ) {
						self.newPechinchaModal.remove() ;
					}) ;
				}
			}
		}
		
		// Inicializa o Controller
		self.init = {
			initialized: false, 
			run: function() {
				// Recupera a lista de Pechinchas do banco de dados
				PechinchaModel.listAll().then( function( ret ){
					self.Pechinchas = ret.objects ;
					self.Pechincha = {} ;

					// Marca o App como inicializado
					self.init.initialized = true ;
				}) ;
			}
		}

		// Atualiza a view caso não tenha sido executado o evento beforeEnter
		if( ! self.init.initialized ) { self.init.run() ; }
	}])
})() ;
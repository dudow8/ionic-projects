( function(){
	// Recupera o library de controller
	angular.module( "pechincha.controller" )

	// Instancia do controller
	.controller( 'AppController', 
		[ '$scope', '$state', '$database', '$localstorage', 'DBQuery', '$ionicPopup', '$ionicModal',
	function( $scope, $state, $database, $localstorage, DBQuery, $ionicPopup, $ionicModal )
	{
		// Variável para referencia deste objeto
		var self = this ;

		// Caso o aplicativo esteja sendo executado pela primeira vez
		if( $localstorage.get( 'REGISTERED', false ) == false ) {
			// Buffer para todas as Queries
			var dbsql = [] ;
			
			// Query para remover o database
			dbsql = dbsql.concat( DBQuery.dropSql() ) ;
			// Query para criar o database
			dbsql = dbsql.concat( DBQuery.createSql() ) ;

			// Executa toda a Query
			$database.bath( dbsql, function( sqlStatus ) {
				// Verifica se ocorreram erros
				if( ! sqlStatus.error ) {
					// Define as configurações do sistema
					$localstorage.set( 'REGISTERED', 'YES' ) ;
					console.log( 'aca' ) ;
				}
			}) ;
		}

		// Redireciona para a página inicial
		if( $state.current.name != 'h-app.h-tabs.home' )
			$state.go( 'h-app.h-tabs.home', {}, { location: "replace" } ) ;

		// Atualiza a view toda vez que ela é aberta
		// 	[ Usado para capturar a informação no CacheService ]
		$scope.$on( "$ionicView.enter", function() {
			// Executa caso o estado seja igual a home
			if( $state.current.name == "h-app.h-tabs.home" ) {
				
			}
		}) ;
	}]) ;
})() ;
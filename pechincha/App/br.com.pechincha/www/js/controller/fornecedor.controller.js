( function() {
	// Recupera o library de controller
	angular.module( "pechincha.controller" )
	
	// Instancia do controller
	.controller( 'FornecedorController', 
		[ '$scope', '$pickup', '$cordovaGeolocation', '$ionicModal', '$ionicPopup', 'CacheService', 'FornecedorModel', 
	function( $scope, $pickup, $cordovaGeolocation, $ionicModal, $ionicPopup, CacheService, FornecedorModel )
	{
		// Referência para o Controller ser reconhecido por métodos fora Contexto
		var self = this ;
		
		// A quantidade de zeros será a quantidade da precisão
		// 	[ Verificar a possibilidade de configurar a distancia ]
		var precision = 100 ;
		
		// Container para o geoloc
		var geoloc = { onGPS: false } ;
		
		// Buffer da lista dos Fornecedores
		self.Fornecedores = [] ;

		// Buffer do registro de um Fornecedor
		self.Fornecedor = {} ;

		// Encontra o local via GPS
		self.findGeolocation = function() {
			// Faz a solicitação da localização atual
			$cordovaGeolocation.getCurrentPosition({
				timeout: 30000, 
				enableHighAccuracy: true
			}).then(
				function( position ) {
					// Formata e armazena os dados do Geoloc
					geoloc = {
						onGPS: true,
						lastPosition: position,
						nestedPoint: {
							latitude: ( parseInt( ( position.coords.latitude * precision ) ) / precision ),
							longitude: ( parseInt( ( position.coords.longitude * precision ) ) / precision ),
						}
					} ;
				}, 
				function( err ) {
					// Não foi possível capturar o GPS
				}
			) ;
		}
		
		// Métodos para pickup
		self.pick = {
			// Modal para inclusão do Registro
			new: function() {
				$ionicModal.fromTemplateUrl( 'view/app/fornecedor-form.html', { 
					animation: 'slide-in-up',
					scope: $scope,
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Reseta o formulário
					self.Fornecedor = {} ;
					
					// Exibe o modal
					self.newFornecedorModal = modal ;
					self.newFornecedorModal.show() ;

					// Configura o ambiente do pickup caso não exista callback
					$pickup.putcallback( 'calbackFornecedor', function( persisted ){
						self.init.run() ;
					}) ;
				});
			},
			// Fehca Modal e retorna o Registro para o calback
			select: function( picked ) {
				$pickup.callback( 'calbackFornecedor', picked ) ;

				// Fecha o modal sem seleção
				$pickup.modal( 'pickUpFornecedor' ).hide().then( function( modal ) {
					$pickup.remove( 'pickUpFornecedor' ) ;
				}) ;
			},
			// Modal para edição do Registro
			update: function( object ) {
				$ionicModal.fromTemplateUrl( 'view/app/fornecedor-form.html', { 
					animation: 'slide-in-up',
					scope: $scope,
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Reseta o formulário
					angular.copy( object, self.Fornecedor ) ;
					// self.Fornecedor = object ;
					
					// Exibe o modal
					self.newFornecedorModal = modal ;
					self.newFornecedorModal.show() ;

					// Configura o ambiente do pickup
					$pickup.putcallback( 'calbackFornecedor', function( persisted ) {
						self.init.run() ;
					}, true ) ;
				});
			},
			// Modal para visão do Registro
			remove: function() {
				if( ! self.Fornecedor.Locked ) {
					$ionicPopup.confirm({
						title: 'Atenção',
     					template: 'Confirma a remoção do Registro ?'
					}).then( function( confirm ){
						if( confirm ) {
							FornecedorModel.remove( self.Fornecedor, function( isremoved ) {
								if( isremoved ) {
									self.newFornecedorModal.hide().then( function( modal ) {
										self.newFornecedorModal.remove() ;
										self.init.run() ;
									}) ;
								}
								else {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível remover o Registro. Tente Novamente !"
									});
								}
							}) ;
						}
					}) ;
				}
				else
				{
					$ionicPopup.alert({
					    title: 'Atenção',
					    subTitle: 'O objeto não pode ser removido pois está sendo referenciado por outros registros'
					});
				}
			},
			// Fecha os detalhes da Rota e a inicia, caso marcada
			persist: function( frForm )
			{
				// Caso exista formulário para persistir os dados
				if( frForm )
				{
					// Container para os campos com erro
					var eFields = [] ;
					
					if( ! frForm.Nome.$valid ) eFields.push( "Fornecedor" ) ;
					
					// Verifica se o formulário é válido
					if( ! eFields.length )
					{
						// Caso seja para fazer update
						if( self.Fornecedor.CodFornecedor )
						{
							// Remove as informações desnecessárias para o form
							delete self.Fornecedor.Locked ;
							// Realiza o update
							FornecedorModel.update( self.Fornecedor, 
								function( persisted ){
									$pickup.callback( 'calbackFornecedor', persisted ) ;
									self.newFornecedorModal.hide().then( function( modal ) {
										self.newFornecedorModal.remove() ;
									}) ;
								}
							) ;
						}
						// Caso seja para fazer inclusão
						else
						{
							self.Fornecedor.Geoloc = ( geoloc.onGPS ? geoloc.lastPosition.coords.latitude : "" ) + "|" + ( geoloc.onGPS ? geoloc.lastPosition.coords.longitude : "" ) ;
							self.Fornecedor.Geoloc = self.Fornecedor.Geoloc == '|' ? 'Indisponível' : self.Fornecedor.Geoloc ;
							
							// Realiza o insert
							FornecedorModel.insert( self.Fornecedor, 
								function( persisted ){
									// Fecha o modal e retorna como selecionado o ítem recém persistido
									self.newFornecedorModal.hide().then( function( modal ) {
										$pickup.callback( 'calbackFornecedor', persisted ) ;
										$pickup.remove( 'pickUpFornecedor' ) ;
										self.newFornecedorModal.remove() ;
									}) ;
								}, 
								function() {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível incluir o Registro. Tente Novamente !"
									});
								}
							) ;
						}
					}
					else
					{
						$ionicPopup.alert({
						    title: 'Formulário com Erro',
						    subTitle: 'Por favor, verifique os campos: ' + eFields.join( ', ' )
						});
					}
				}
				// Caso o formulário seja falso, descarta o form ou objeto em edição
				else
				{
					// Fecha o modal e cancela a operação
					self.newFornecedorModal.hide().then( function( modal ) {
						self.newFornecedorModal.remove() ;
					}) ;
				}
			}
		}
		
		// Inicializa o Controller
		self.init = {
			initialized: false, 
			run: function() {
				// Recupera a lista de Fornecedores do banco de dados
				FornecedorModel.listAll().then( function( ret ){
					self.Fornecedores = ret.objects ;
					self.Fornecedor = {} ;

					// Marca o App como inicializado
					self.init.initialized = true ;
				}) ;

				// Recupera a posição do GPS para o formulário de inclusão
				self.findGeolocation() ;
			}
		}

		// Atualiza a view caso não tenha sido executado o evento beforeEnter
		if( ! self.init.initialized ) { self.init.run() ; }

		// Atualiza a view toda vez que ela é aberta
		$scope.$on( "$ionicView.beforeEnter", function() {
			self.init.run() ;
		}) ;
	}])
})() ;